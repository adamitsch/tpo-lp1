
# Predlog projekta

| | |
|:---|:---|
| **Naziv projekta** | Spletna trgovina računalniške opreme |
| **Člani projektne skupine** | Jan Adamič, Jan Lovšin, Mihael Rajh, Dejan Sinic |
| **Kraj in datum** |Ljubljana, 17.3.2019


## Povzetek projekta

Glavni cilj je implementirati spletno trgovino, ki je namenjena celotnemu spektru uporabnikov, od manj do bolj zahtevnih. Vsebovala bo različne računalnike in računalniške komponente. Spletna stran bo nudila polnjenje košarice s produkti brez registracije. Če uporabnik želi z nakupom nadaljevati kasneje, se lahko registrira, pri čemer se mu košarica ohrani za poznejšo rabo. Za izvršitev nakupa pa se enostavno odda naročilo z vsemi izdelki iz košarice.

Naš osebni cilj je utrditi oziroma dopolniti znanje spletnega programiranje z uporabo MEAN sklada tehnologij s poudarkom na zasnovi projekta. Člani skupine bomo tako razdeljeni na enakomerno delo na večih delih spletne strani, odvisno od predznanja in želj.


## 1. Motivacija


V današnjem času vedno več nakupovanja poteka preko spleta. Študija v Veliki Britaniji je pokazala, da 51% ljudi raje nakupuje po spletu kot pa fizično. Da se nakupovanje v veliki meri seli na splet kaže tudi dejstvo, da je bilo leta 2018 opravljenega 2842 milijard prometa preko spleta. Do leta 2021 bi se naj ta številka povzpela na 4878 milijard.

Konkurenca je vedno večja in ker želja po kvalitetnih in hkrati privlačnih spletnih straneh rase, smo se odločili, da ustvarimo uporabnikom prijazno spletno stran za prodajo računalnikov in računalniške opreme. Primarno želimo ustvariti spletno stran, ki se bo držala načela “Less in More” glede dizajna. Tukaj moramo ohraniti ravnotežje med uporabniško izkušnjo in podano količino informacije o samih izdelkih, da zadostimo zahtevnejšim strankam. S tem ravnotežjem želimo pritegniti prav celotni spekter uporabnikov, ki kupujejo računalniško opremo, od manj zahtevnega, ki ga primarno privabi dizajn in intuitivna spletna stran, do zahtevnejšega, ki mogoče išče hitre, kvalitetne in celostne informacije o izdelkih preden se odloči za nakup.


## 2. Cilji projekta in pričakovani rezultati

### 2.1 Opis ciljev

Naš izdelek bo uporabniku ponujal pester izbor računalnikov in računalniških komponent. Pri tem bo omogočal uporabo košarice brez registracije. Ob zaključku izbire produktov bo imel možnost izvršitve nakupa ob registraciji. Pri implementaciji spletne strani se bomo posluževali MEAN sklada tehnologij.


### 2.2 Pričakovani rezultati

Pričakovani rezultat je spletna stran, podprta na različnih napravah, ki omogočajo spletno brskanje, od manjših telefonov, do večjih računalnikov in tablic. Omogočala bo uporabo košarice, registracije in izvršitve nakupa. Dodatno bodo registrirani uporabniki lahko podali svojo mnenje o izdelkih v obliki komentarjev in ocene izdelka. Da zadovoljimo zahtevnejše uporabnike bodo izdelki vsebovali večjo količino informacij o svojih karakteristikah. Ob tem pa bomo tudi poskrbeli za manj zahtevne stranke, ki jih pa mogoče bolj pritegne sam izgled trgovine. Za implementacijo take spletne strani bomo uporabljali MEAN sklad tehnologij.


## 3. Projektni načrt

### 3.1 Povzetek razdelitve projekta na aktivnosti

 Najprej se bodo izvedle aktivnosti, ki se nanašajo na organizacijo in planiranje projekta. Tu spada tudi načrt obvladovanja tveganj, načrtovanje programske opreme in prototipiranje spletne strani. Temu sledi sama izvedba programiranja in testiranja aplikacije. V zadnjem delu projekta sledita še dokumentacija in predstavitev.
 

### 3.2 Načrt posameznih aktivnosti

Za projekt je na voljo 2 meseca, kar nanese na 45 delovnih dni, nevključujoč vikende. S štirimi člani skupine imamo na voljo 8 ČM, kjer je 1 ČM = 60h. Dnevna obremenitev je približno 0.18 ČM/dan, oz. 10.5 h/dan (2.5h na osebo).

---

| **Oznaka aktivnosti** | A1 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 25. 03. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 27. 03. 2019  |
| **Trajanje** | 3 dni |
| **Naziv aktivnosti** | Zbiranje in zajem zahtev. |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Iskanje zahtev projekta. Sestava seznama funkcionalnih in nefunkcionalnih zahtev. |
| **Opis aktivnosti** | Člani sestavijo seznam funkcionalnosti, ki so bodisi zahtevane bodisi zaželjene v končnem izdelku. |
|**Morebitne odvisnosti in omejitve** | Aktivnost A1 je prva aktivnost in nima odvisnosti. Je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Seznam funkcionalnosti in zahtev projekta. |

---

| **Oznaka aktivnosti** | A2 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 01. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 05. 04. 2019  |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Sestava specifikacije zahtev. |
| **Obseg aktivnosti v ČM** | 0.8 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Urejanje zahtev. Kategorizacija zahtev. Sestava specifikacije zahtev. |
| **Opis aktivnosti** | Seznam funkcionalnosti, ki je bil sestavljen v prejšnjem koraku, se uredi in formalizira v specifikacijo zahtev. |
|**Morebitne odvisnosti in omejitve** | Aktivnost A2 je odvisna od aktivnosti A1 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Specifikacija zahtev. |

---

| **Oznaka aktivnosti** | A3 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 08. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 12. 04. 2019  |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Načrtovanje podatkovne baze. |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Izdelava konceptualnega načrta PB. Izdelava logičnega načrta PB. Normalizacija PB. |
| **Opis aktivnosti** | Izdela se konceptualni model podatkovne baze. Z logičnim načrtovanjem se ta pretvori v logični model in nato normalizira. |
|**Morebitne odvisnosti in omejitve** | Aktivnost A3 je odvisna od aktivnosti A2 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Normaliziran načrt podatkovne baze. |

---

| **Oznaka aktivnosti** | A4 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 28. 03. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 04. 04. 2019 |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Trajanje** | 8 dni |
| **Naziv aktivnosti** | Usposabljanje v načrtovanju. |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Pridobitev znanja o zbiranju zahtev. Pridobitev osnov načrtovanja. |
| **Opis aktivnosti** | Na predavanjih in vajah predmeta TPO se člani izobrazijo o postopku zbiranja zahtev in načtovanju programske opreme. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A4 nima odvisnosti in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Izboljšano znanje o načrtovanju. |

---

| **Oznaka aktivnosti** | A5 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 08. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 15. 04. 2019  |
| **Trajanje** | 8 dni |
| **Naziv aktivnosti** | Izdelava načrta programskih modulov. |
| **Obseg aktivnosti v ČM** | 0.8 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Določitev programskih modulov. Določitev odvisnosti med moduli. Sestava strukturnega diagrama. |
| **Opis aktivnosti** | Zajete zahteve in funkcionalnosti se pretvorijo v ustrezujoče programske module. Sestavi se strukturni diagram, ki prikazuje sestavo končnega izdelka. |
|**Morebitne odvisnosti in omejitve** | Aktivnost A5 je odvisna od aktivnosti A2 in A4 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt programskih modulov. |

---

| **Oznaka aktivnosti** | A6 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 16. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 19. 04. 2019  |
| **Trajanje** | 4 dni |
| **Naziv aktivnosti** | Izdelava načrta testiranja. |
| **Obseg aktivnosti v ČM** | 0.6 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Določitev testov. Izdelava načrta testiranja. Določitev poteka testiranja. |
| **Opis aktivnosti** | Določijo se testi, ki se bodo izvedli v okviru testiranja. Izdela se načrt, ki določa časovni potek in obseg posameznih testov. |
|**Morebitne odvisnosti in omejitve** | Aktivnost A6 je odvisna od aktivnosti A5 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Načrt testiranja.


---

| **Oznaka aktivnosti** | A7 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 04. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Izdelava Wireframe oz. JustInMind prototipa. |
| **Obseg aktivnosti v ČM** | 0.3 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Sestava Wieframe prototipa. Smernice za lažjo orientacijo pri kreiranju front-end sistema. |
| **Opis aktivnosti** | Uporabi se predhodno določena programska oprema (balsamiq, JustInMind, ...), kjer člani realiziramo svojo idejo o izgledu spletne strani, ter opišemo glavne ter alternativne toke. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A7 je odvisna od aktivnosti A5 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Oprijemljiv Wireframe prototip. |

---

| **Oznaka aktivnosti** | A8 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 29. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 08. 05. 2019 |
| **Trajanje** | 10 dni |
| **Naziv aktivnosti** | Implementacija front-end (čelnega) sistema. |
| **Obseg aktivnosti v ČM** | 0.8 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Delujoči front-end sistem. |
| **Opis aktivnosti** | Front-end spletne strani je implementiran s pomočjo tehnologij html, css in javscript. Pri tem bo uporabljen Angular. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A8 sledi aktivnosti A7 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Implementiran front-end (čelni) del spletne strani |

---

  | **Oznaka aktivnosti** | A9 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 09. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** |  10. 05. 2019 |
| **Trajanje** | 2 dni |
| **Naziv aktivnosti** | Testiranje front-end sistema. |
| **Obseg aktivnosti v ČM** | 0.2 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Iskanje ter odpravljanje napak. |
| **Opis aktivnosti** | Stran se najprej pretestira z osnovnimi primeri. Če so uspešni se poskusi robne primere. Pri odkritju hroščev, se jih popravi in ponovno testira. Poskrbimo, da spletna stran deluje po pričakovanju na različnih napravah.  |
| **Morebitne odvisnosti in omejitve** | Aktivnost A9 je odvisna od aktivnosti A6 in A8 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Uspešno opravljeno testiranje. |

---

  | **Oznaka aktivnosti** | A10 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 25. 04. 2019 |
| **Trajanje** | 4 dni |
| **Naziv aktivnosti** | Implementacija artiklov in komentarjev. |
| **Obseg aktivnosti v ČM** | 0.2 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Uporabniški vmesnik ima zmožnost dodajanja novih in urejanja starih artiklov. |
| **Opis aktivnosti** | Implementira se dodajanje in urejanje artiklov. Uporabnik ima na voljo dodatno stran za dodajanje/urejanje. | |
| **Morebitne odvisnosti in omejitve** |  Aktivnost A10 je odvisna od aktivnosti A3 in A5 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Možnost dodajanja in brisanja artiklov. |

---

  | **Oznaka aktivnosti** | A11 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 22. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 26. 04. 2019 |
| **Trajanje** |  5 dni |
| **Naziv aktivnosti** | Implementacija prijave in registracije. |
| **Obseg aktivnosti v ČM** | 0.4 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Možnost registracije in prijave v sistem. |
| **Opis aktivnosti** | Na strežniku se implementira registracija ter prijava. Uporabnik ima na voljo dodatno stran za registracijo/prijavo, odvisno od trenutnega stanja. Seveda lahko brska brez prijave, za nakup pa je potrebna.  |
| **Morebitne odvisnosti in omejitve** | Aktivnost A11 je odvisna od aktivnosti A3 in A5 in je na kritični poti.  |
| **Pričakovani rezultati aktivnosti** | Uporabnik se lahko registrira in prijavi. |

---

  | **Oznaka aktivnosti** | A12 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 29. 04. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 03. 05. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Implementacija košarice. |
| **Obseg aktivnosti v ČM** | 0.4 ČM|
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Delujoča košarica. Uporabnik lahko sestavi seznam željenih artiklov. |
| **Opis aktivnosti** | Vsak produkt je možno kupiti s klikom na kupi. Ob kliku se stvar doda v košarico. Za košarico je na voljo svoja stran, kjer lahko uporabnik ureja izdelke ali konča nakup. |
| **Morebitne odvisnosti in omejitve** |  Aktivnost A12 je odvisna od aktivnosti A10 in A11 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Funkcionalna košarica. |

---

  | **Oznaka aktivnosti** | A13 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 06. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 10. 05. 2019 |
| **Trajanje** | 5 dni |
| **Naziv aktivnosti** | Implementacija obdelave naročil. |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Omogočene so transakcije. Podpora obveščanja. |
| **Opis aktivnosti** | Uporabniku omogočimo izvršitev nakupa, na email se mu posredujejo podatki o uspešnem naročilu, naročilo se doda v bazo, zaloge se posodobijo. |
| **Morebitne odvisnosti in omejitve** |  Aktivnost A13 sledi aktivnosti A12 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Omogočena izvršitev nakupa. |

---

  | **Oznaka aktivnosti** | A14 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 13. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 14. 05. 2019 |
| **Trajanje** | 2 dni |
| **Naziv aktivnosti** | Testiranje back-end (zalednega) sistema. |
| **Obseg aktivnosti v ČM** | 0.2 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Odpravitev napak v zalednem sistemu. |
| **Opis aktivnosti** | Zaledni sistem se pretestira z osnovnimi primeri, nato še z robnimi primeri. Napake se sproti popravijo in sistem se ponovno testira.  |
| **Morebitne odvisnosti in omejitve** | Aktivnost A14 je odvisna od aktivnosti A6 in A13 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Uspešno opravljeno testiranje. |

---

  | **Oznaka aktivnosti** | A15 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 15. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 17. 05. 2019 |
| **Trajanje** | 3 dni |
| **Naziv aktivnosti** | Integracija back-end in front-end sistemov. |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Integracija čelnega in zalednega sistema v zaključeno celoto. |
| **Opis aktivnosti** | Pri tej aktivnosti se bo združilo funkcionalnosti back-enda z uporabniškim vmesnikom. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A15 sledi aktivnostim A9 in A14 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Pravilno delovanje spletne strani. |

---

  | **Oznaka aktivnosti** | A16 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 20. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 22. 05. 2019 |
| **Trajanje** | 3 dni |
| **Naziv aktivnosti** | Testiranje celotne aplikacije. |
| **Obseg aktivnosti v ČM** | 0.4 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Cilj je realizirana spletna stran, ki je pripravljena za uporabo. |
| **Opis aktivnosti** | Pregled glavnih in alternativnih tokov. Preverjanje delovanja sej, uporabe košarice in izvrševanja nakupov. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A16 sledi aktivnosti A15 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Uspešno opravljeno testiranje. |

---

  | **Oznaka aktivnosti** | A17 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 23. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 24. 05. 2019 |
| **Trajanje** | 2 dni |
| **Naziv aktivnosti** | Priprava dokumentacije. |
| **Obseg aktivnosti v ČM** | 0.3 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Ustvari se dokument, kjer so podane tehnologije, postopki razvoja, ugotovitve in mnenja. Ustvarijo se tudi PU-ji (Primeri uporabe), kjer so vsebovani osnovni ter alternativni tokovi pri uporabi spletne strani. |
| **Opis aktivnosti** | Pisanje dokumentacije in PU-jev. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A17 sledi aktivnosti A16 in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Ker želimo olajšati delo na projektu morebitnim bodočim razvijalcem v primeru nadgradnje, ustvarimo dokument, namenjen razvijalcem in vodji projekta, da so na tekočem o predhodnem delu na projektu. PU-je namenimo QA(Quality Assurance) inženirjem, da se seznanijo z vsemi aspekti spletne strani. |

---

  | **Oznaka aktivnosti** | A18 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 23. 05. 2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** |  23. 05. 2019 |
| **Trajanje** | 1 dan |
| **Naziv aktivnosti** | Priprava predstavitve. |
| **Obseg aktivnosti v ČM** | 0.2 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Priprava zanimive predstavitve. Glavne iztočnice projekta. Kaj smo se naučili. |
| **Opis aktivnosti** | Zajem vseh glavnih funkcionalnosti projekta. Izpostava glavnih problemov, ki smo jih utrpeli in načini rešitev.  |
| **Morebitne odvisnosti in omejitve** | Aktivnost A18 sledi aktivnosti A16 in ni na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Uspešno pripravljena predstavitev. |

---

  | **Oznaka aktivnosti** | A19 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | 25.03.2019 |
| **Predvideni datum zaključka izvajanja aktivnosti** | 24.05.2019 |
| **Trajanje** | 61 dni |
| **Naziv aktivnosti** | Vodenje projekta in obvladovanje tveganj. |
| **Obseg aktivnosti v ČM** | 0.6 ČM |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | Sledenje zadani zasnovi implementacije projekta, ob zagotavljanju kvalitete in dosego zadanih rokov. |
| **Opis aktivnosti** | Izvedba načrtovanega projekta. |
| **Morebitne odvisnosti in omejitve** | Aktivnost A19 nima odvisnosti in je na kritični poti. |
| **Pričakovani rezultati aktivnosti** | Organizacija dela na projektu in načrt za obvladovanje tveganj. |

### 3.3 Seznam izdelkov


| Oznaka izdelka | Ime izdelka | Datum izdaje |
| :--- | :---------------------------- | :--- |
| PC 2.1 | Specifikacije projekta | 27. 03. 2019 |
| PC 3.1 | Načrt podatkovne baze | 12. 04. 2019 |
| PC 5.1 | Načrt programskih modulov | 15. 04. 2019 |
| PC 1.2 | Načrt testiranja | 19. 04. 2019 |
| PC 7.1 | JustInMind prototip aplikacije | 26. 04. 2019 |
| PC 9.1 | Front-end aplikacije | 10. 05. 2019 |
| PC 14.1 | Back-end aplikacije | 14. 05. 2019 |
| PC 16.1 | Končana aplikacija | 22. 05. 2019 |
| PC 17.1 | Dokumentacija | 24. 05. 2019 |
| PC 18.1 | Končna predstavitev | 23. 05. 2019 |

  
### 3.4 Časovni potek projekta - Ganttov diagram

![Ganttov diagram](../img/ganttov-diagram.png)


### 3.5 Odvisnosti med aktivnosti - Graf PERT

![Graf PERT](../img/graf-pert.png)

Kritična pot je na grafu označena z odebeljenimi puščicami. Polja v kvadratkih pa pomenijo:

| oznaka aktivnosti | | |
|:------------- |:------------- |:------------- |
| trajanje aktivnosti | čas najhitrejšega začetka | čas najhitrejšega začetka |
| koliko dni se lahko zamakne projekt  | čas najpoznejšega začetka | čas najpoznejšega konca|

## 4. Obvladovanje tveganj

  

### 4.1 Identifikacija in analiza tveganj



| Naziv tveganja | Opis tveganja | Tip tveganja | Verjetnost nastopa tveganja | Posledice nastopa tveganja |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|  Nerazpoložljivost strojne opreme  |  Cenovnega strežnika s potrebno zmogljivostjo ni na voljo.   |  Orodja |   Zelo visoka |  Katastrofalne  |
| Slabo vodenje | Delo ni enakomerno porazdeljeno,   | Organizacija | Nizka | Katastrofalne  |
| Tveganje varnosti | Aplikacija je podvržena zlonamernemu napadu. | Tehnologija | Zelo nizka | Katastrofalne  |
| Izbira napačne tehnologije | Slaba zmogljivost za določene funkcionalnosti zaradi neprimerne tehnologije.  | Tehnologija | Nizka  | Zelo resne  |
| Podcenjen predviden čas   | Potrebovan čas za razvoj sklopa funkcionalnosti spletne strani je podcenjen.  |  Ocenjevanje |  Visoka  | Resne   |
|  Zahteve se spremenijo  |  Zahteve projekta se spremenijo zaradi drugih potreb.  |  Zahteve |  Visoka  |  Resne  |
|  Nedostopnost člana ekipe | Član ekipe je nedostopen zaradi višje sile.   | Ljudje  |  Srednja  |  Resne  |
| Slaba komunikacija  | Člani ne delijo znanja, med izvajanjem produkte je slabo sodelovanje.  | Organizacija | Nizka  | Resne  |
|  Podcenjena zahtevnost projekta  |  Potrebovano znanje in izkušnje za razvoj sklopa funkcionalnosti spletne strani je podcenjen   |  Ocenjevanje |   Nizka |  Resne  |
|  Odstop člana ekipe  |  Član ekipe odstopi od projekta.  | Ljudje  |  Nizka  |  Resne  |
|  Okvara   | Članu ekipe se pokvari računalnik.   |  Orodja |  Nizka  |  Dopustljive  |
|  Pomankljive zahteve  | Zahteve projekta so premalo specifične.   |  Zahteve |  Nizka  |  Zanemarljive  |

**Legenda za stolpce v zgornji tabeli**

| Naziv stolpca | Možne vrednosti |
| :------------- | :------------ | 
| Tip tveganja | Tehnologija, organizacija, ljudje, orodja, zahteve, ocenjevanje |
| Verjetnost nastopa tveganja | Zelo nizka (< 10%), nizka(10-25%), srednja (25-50%), visoka (50-75%), zelo visoka (> 75%) |
| Posledice nastopa tveganja | Katastrofalne (ogrozi uspešnost projekta), resne (pobzroči večje zamude), dopustljive (zamude so dovoljene glede na urnik), zanemarljive |


### 4.2 Načrtovanje tveganj

  
| Tveganje | Strategija |
| :-------- | :--------- |
| Odpoved delovanja opreme | Pridobi se ponudnika, ki zagotavlja čim večje delovanje strežnika. Poleg tega se uporabi sodobne tehnologije za kontroliranje zdravja. |
| Izbira napačne tehnologije | Pridobi se člane z izkušnjami v industriji. Kot temelej se postavi najboljše predlagane tehnologije. |
|  Slaba komunikacija  |  Člani se med projektom večkrat dobijo in predebatirajo o trenutnih problemih in idejah. Za komunikacijo pa uporabljajo Slack ali Discord za boljše sodelovanje.  |
|   Slabo vodenje |  Sodelovanje vseh članov, izbere se predstavniki, ki ima izkušnje z vodenjem.  |
|  Nedostopnost člana ekipe  |  Več članov je razdeljenih med iste projekte. Odsotnost enega lahko za določen čas nadomešča drugi član na istem procesu, saj ima dovolj znanja in razumevanja o konkretnem procesu.  |
|  Odstop člana ekipe  |  Poskrbi se za zanimive dogodke, dobre razmere in proste ure za razvijalce.  |
|  Nerazpoložljivost strojne opreme  |  Pri razvoju strežnik ne bo preobremenjen, saj se bo lokalno razvijal z le nekaj uporabniki.   |
|  Okvara  |  Vsakemu razvijalcu se ponudi službene računalnike, za katera je poskrbljeno, da delujejo tip-top.  |
|  Zahteve se spremenijo  |  Potrebna dobro definirana osnova, ki je razširljiva za dodatne funkcionalnosti.  |
|  Pomankljive zahteve  |  Poksrbeti je treba, da so čisto osnovne zahteve definirane čim bolj specifično.   |
|  Podcenjen predviden čas  | Bolj natančno se definira osnove projekta in poskuša za probleme, ki so že rešeni, poiskati že izvršene implementacije. |
|  Podcenjena zahtevnost projekta  |  Ustvari se natančen in organiziran potek projekta preko kateregea se lahko prikaže osnovna zahtevnost.  |


## 5. Upravljanje projekta




| **Dejavnost** | **Jan Adamič** | **Jan Lovšin** | **Mihael Rajh** | **Dejan Sinic** | 
| :-------- | :--------- | :-------- | :--------- | :-------- |
| Povzetek projekta | | 35.00%| |65.00%|
| Opis ciljev | | 35.00%| |65.00%|
| Motivacija | |35.00% | |65.00%|
| Cilji projekta in pričakovani rezultati | | | |100.00%|
| Projektni načrt |25.00%| | 50.00% |25.00%|
| Časovna razporeditev aktivnosti | | | 100.00% | |
| Seznam izdelkov | 70.00% | | 30.00%| |
| Ganttov diagram | | 100.00%| | |
| Graf PERT |100.00% | | | |
| Obvladovanje tveganj | 45.00% | 45.00%| 10.00% | |
| Opis skupine | 25.00% | 25.00% | 25.00% | 25.00% |
| Finančni načrt | | | 100.00% | |
  

## 6. Predstavitev skupine

Jan Adamič, 21, študent dodiplomskega programa na Univerzi v Ljubljani, Fakulteti za računalništvo in informatiko. Delal bo predvsem na strežniškem delu in nekaj tudi na čelnem (front-end). Seznanjen je z Javascriptom, MongoDB-jem, HTML-jem in CSS-om.

Jan Lovšin, 22, študent dodiplomskega programa na Univerzi v Ljubljani, Fakulteti za računalništvo in informatiko. Ima veliko znanja z HTML5, CSS, Javscript, Angular ter Boostrap, zaradi česar bo osredotočen na čelni sistem (front-end).

Mihael Rajh, 21, študent dodiplomskega programa na Univerzi v Ljubljani, Fakulteti za računalništvo in informatiko. Najbolje je seznanjen z Javo, JavaScriptom in SQL-om, zato se bo osredotočil na delo z zalednim sistemom (project back-end).

Dejan Sinic, študent dodiplomskega programa na Univerzi v Ljubljani, na Fakulteti za računalništvo in informatiko. Zadostno znanje web stacka in izkušnje z delom na večjem projektu kot QA inženir in ostalo.

## 7. Finančni načrt - COCOMO II ocena

Finančni plan navaja eno delovno uro v vrednosti 6.67 EUR. Ker je en ČM enak 60h, je en ČM vreden 400 EUR.
  
| **Aktivnost** | **Naziv aktivnosti** | **Obseg aktivnosti (ČM)** | **Stroški dela (EUR)** | **Stroški investicij (EUR)** | **Potni stroški (EUR)** | **Posredni stroški (EUR)** | 
| :-------- | :--------- | :-------- | :--------- | :-------- | :-------- | :-------- |
| A1 | Zbiranje in zajem zahtev. | 0.4 | 160 | 0 | 20 | 16 |
| A2 | Sestava specifikacije zahtev. | 0.8 | 320 | 0 | 0 | 32 |
| A3 | Načrtovanje podatkovne baze. | 0.4 | 160 | 0 | 0 | 16 |
| A4 | Usposabljanje v načrtovanju. | 0.4 | 160 | 20 | 40 | 20 |
| A5 | Izdelava načrta programskih modulov. | 0.8 | 320 | 10 | 0 | 33 |
| A6 | Izdelava načrta testiranja. | 0.6 | 240 | 0 | 0 | 24 |
| A7 | Izdelava JustInMind prototipa. | 0.3 | 120 | 10 | 0 | 12 |
| A8 | Implementacija front-end (čelnega) sistema. | 0.8 | 320 | 0 | 0 | 32 |
| A9 | Testiranje front-end sistema. | 0.2 | 80 | 0 | 0 | 8 |
| A10 | Implementacija artiklov in komentarjev. | 0.2 | 80 | 0 | 0 | 8 |
| A11 | Implementacija prijave in registracije. | 0.2 | 80 | 0 | 0 | 8 |
| A12 | Implementacija košarice. | 0.4 | 160 | 0 | 0 | 16 |
| A13 | Implementacija obdelave naročil. | 0.4 | 160 | 20 | 0 | 18 |
| A14 | Testiranje back-end (zalednega) sistema. | 0.2 | 80 | 0 | 0 | 8 |
| A15 | Integracija back-end in front-end sistemov. | 0.4 | 160 | 20 | 0 | 18 |
| A16 | Testiranje celotne aplikacije. | 0.4 | 160 | 0 | 0 | 16 |
| A17 | Priprava dokumentacije. | 0.3 | 120 | 10 | 0 | 12 |
| A18 | Priprava predstavitve. | 0.2 | 80 | 10 | 20 | 10 |
| A19 | Vodenje projekta in obvladovanje tveganj. | 0.6 | 240 | 40 | 20 | 30 |
| Skupno | Izdelava projekta. | 8.0 | 3200 | 140 | 100 | 337 |



![COCOMO II ocena](https://i.imgur.com/Dnco91u.jpg)

  
Skupno imamo 8 ČM in 3440 neposrednih stroškov (delo, investicije, potni stroški). Od teh 3440 je potrebno odšteti 6 EUR, skladno z izračunom COCOMOII modela. Nato se v skladu z dobro prakso dodajo posredni stroški v vrednosti 10%, kar nam da celoten budget 3777 EUR.
  

## Reference

[1]: Ecommerce News europe (https://ecommercenews.eu/51-uk-consumers-prefer-to-shop-online-than-in-store/)

[2]: Online tool for Gantt graphs (https://www.teamgantt.com/)

[3]: Statistics and Studies from more than 22,500 Sources (https://www.statista.com/topics/871/online-shopping/)

[4]: COCOMO II - Constructive Cost Model (http://csse.usc.edu/tools/COCOMOII.php)

[5]: SHARE PPT & PDF FILES (https://www.slideserve.com/tangia/razvoj-informacijskih-sistemov)

[6]: Simplicable Guide (https://simplicable.com/new/technology-risk)
